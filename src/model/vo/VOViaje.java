package model.vo;

public class VOViaje 
{
	/**
	 * Modela el id del viaje
	 */
	private String idViaje;

	private String idServicio;
	
	private String idRuta;
	
	public String getIdServicio(){
		return idServicio;
	}
	
	public void setIdServicio(String pId){
		idServicio = pId;
	}
	
	public String getIdRuta(){
		return idRuta;
	}
	
	public void setIdRuta(String pId){
		idRuta = pId;
	}
	
	/**
	 * @return the idViaje
	 */
	public String getIdViaje()
	{
		return idViaje;
	}

	/**
	 * @param idViaje the idViaje to set
	 */
	public void setIdViaje(String idViaje)
	{
		this.idViaje = idViaje;
	}
	
	

}
