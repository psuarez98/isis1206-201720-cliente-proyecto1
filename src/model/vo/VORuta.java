package model.vo;

import model.data_structures.IList;

public class VORuta 
{
	//Atributos
	
	/**
	 * Modela el id de la ruta
	 */
	 private String idRoute;

	 /**
	  * Model el short name de la ruta
	  */
	 private String shortName;
	 
	 private String agencyId;
	 
	 private VOServicio servicio;
	 
	 //M�todos
	 
	 public String getAgencyId(){
		 return agencyId;
	 }
	 
	 public void setAgencyId(String pId){
		 agencyId = pId;
	 }
	 
	/**
	 * @return the idRoute
	 */
	public String getIdRoute() 
	{
		return idRoute;
	}

	/**
	 * @param idRoute the idRoute to set
	 */
	public void setIdRoute(String idRoute) 
	{
		this.idRoute = idRoute;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() 
	{
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName)
	{
		this.shortName = shortName;
	}
	
	/**
	 * @author cv.trujillo
	 */
	public IList <VORetardoViaje> getSchedules()
	{
		return servicio.getListaViajeRetardado();
	}
}
