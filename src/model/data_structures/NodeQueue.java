package model.data_structures;

public class NodeQueue<T>{
	//En esta clase hacemos lo mismo que en la clase NodeStack pero en �sta parte no vamos a tener la referencia al nodo anterior.
	
	//--------------------------------------------------------------
	//Atributos
	//--------------------------------------------------------------
	
	/**
	 * Elemento que contiene el nodo
	 */
	private T element;
	
	/**
	 * Siguiente nodo al que est� encadenado	
	 */
	private NodeQueue<T> next;
	

	
	/**
	 * Identificador del nodo. En cualquier lista, los id son �nicos
	 * La lista es la que se va a encargar de que se cumpla esta condici�n
	 */
	private String id;

	public NodeQueue anterior;
	
	
	//--------------------------------------------------------------
	//Constructor
	//--------------------------------------------------------------
	
	/**
	 * Constructor para un unico nodo
	 * @param pElement Elemento que va a contener el nodo
	 * @param pId Identificador del Nodo
	 */
	public NodeQueue(T pElement, String pId){
		
		element = pElement;
		
		id = pId;
		
		next = null;
		
		
	}
	
	//--------------------------------------------------------------
	//Metodos
	//--------------------------------------------------------------
	
	/**
	 * @return Retorna el elemento guardado en el nodo
	 */
	public T darElemento(){
		return element;
	}
	
	/**
	 * @return Retorna el identificador del nodo
	 */
	public String darId(){
		return id;
	}
	
	/**
	 * @return Retorna el siguiente nodo
	 */
	public NodeQueue<T> darSig(){
		return next;
	}
	

	
	/**
	 * Cambia el nodo anterior
	 * @param pNodo Nuevo nodo anterior
	 * @return True si la operaci�n funcion�. False en caso contrario
	 */
	public boolean cambiarAnt(NodeQueue<T> pNodo){
		try{
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	/**
	 * Cambia el nodo siguiente
	 * @param pNodo Nuevo nodo siguiente
	 * @return True si funcion�. False en caso contrario
	 */
	public boolean cambiarSig(NodeQueue<T> pNodo){
		try{
			next = pNodo;
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
