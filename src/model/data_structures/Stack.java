package model.data_structures;

public class Stack<T> implements IStack<T>{
	
	//---------------------------------------------------------
	//Atributos
	//---------------------------------------------------------
	
	private NodeStack<T> last;

	private int size;
	
	//---------------------------------------------------------
	//Constructor
	//---------------------------------------------------------
	/**
	 * Construye una pila vac�a
	 */
	public Stack(){
		last = null;
		size = 0;
	}
	

	//---------------------------------------------------------
	//Metodos
	//---------------------------------------------------------
	
	/**
	 * Devuelve el tama�o del stack
	 * @return size
	 */
	public int getSize(){
		return size;
	}
	
	/**
	 * Devuelve el elemento contenido en la cima de la pila sin sacarlo
	 * @return null si la pila est� vac�a, o si size > 0 devuelve el elemento de la cima
	 */
	public T getTop(){
		if(size == 0)
			return null;
		else
			return last.darElemento();
	}
	
	public boolean isEmpty(){
		if(size == 0)
			return true;
		else
			return false;
	}
	
	/**
	 * Agrega un nuevo elemento a la cima de la pila
	 */
	@Override
	public void push(T item) {
		NodeStack<T> nuevo = new NodeStack<T>(item, String.valueOf(item));
		if(size == 0)
			last = nuevo;
		else{
			nuevo.cambiarAnt(last);
			last = nuevo;
		}
		size ++;			
	}
	
	/**
	 * Saca el elemento en la cima de la pila
	 * @return El elemento en la cima de la pila. NULL si la pila est� vac�a
	 */
	@Override
	public T pop() {
		if(size == 0)
			return null;
		else{
			T dar = last.darElemento();
			last = last.darAnt();
			size --;
			return dar;
		}			
	}

}
