package model.data_structures;

public class NodeStack<T>{
	
	//--------------------------------------------------------------
	//Atributos
	//--------------------------------------------------------------
	
	/**
	 * Elemento que contiene el nodo
	 */
	private T element;
	
	/**
	 * Anterior nodo al que est� encadenado
	 */	
	private NodeStack<T> prev;
	
	/**
	 * Identificador del nodo. En cualquier lista, los id son �nicos
	 * La lista es la que se va a encargar de que se cumpla esta condici�n
	 */
	private String id;
	
	
	//--------------------------------------------------------------
	//Constructor
	//--------------------------------------------------------------
	
	/**
	 * Constructor para un unico nodo
	 * @param pElement Elemento que va a contener el nodo
	 * @param pId Identificador del Nodo
	 */
	public NodeStack(T pElement, String pId){
		
		element = pElement;
		
		id = pId;
		
		prev = null;
		
	}
	
	//--------------------------------------------------------------
	//Metodos
	//--------------------------------------------------------------
	
	/**
	 * @return Retorna el elemento guardado en el nodo
	 */
	public T darElemento(){
		return element;
	}
	
	/**
	 * @return Retorna el identificador del nodo
	 */
	public String darId(){
		return id;
	}
	
	/**
	 * @return Retorna el anterior nodo
	 */
	public NodeStack<T> darAnt(){
		return prev;
	}
	
	/**
	 * Cambia el nodo anterior
	 * @param pNodo Nuevo nodo anterior
	 * @return True si la operaci�n funcion�. False en caso contrario
	 */
	public boolean cambiarAnt(NodeStack<T> pNodo){
		try{
			prev = pNodo;
			return true;
		}catch(Exception e){
			return false;
		}
	}
}

