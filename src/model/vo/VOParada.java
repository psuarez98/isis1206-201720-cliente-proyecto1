package model.vo;

public class VOParada 
{
	//Atributos
	/**
	 * Modela el id de la parada
	 */
	private String stopId;
	
	/**
	 * Modela el n�mero de incidentes
	 */
	private int numeroIncidentes;
	
	private String stopNom;
	
	private String stopCode;
	
	private double stopLat;
	
	private double stopLon;
	
	private StopUpdateVO update;
	
	private String stopDesc;
	
	//M�todos
	public void setStopDesc(String pD){
		stopDesc = pD;
	}
	
	public String getStopDesc(){
		return stopDesc;
	}
	
	public void setUpdate(StopUpdateVO pUpdate){
		update = pUpdate;
	}
	
	public StopUpdateVO getUpdate(){
		return update;
	}
	
	public void setStopCode(String pStopCode){
		stopCode = pStopCode;
	}
	
	public String getStopCode(){
		return stopCode;
	}
	
	public void setLon(double pLon){
		stopLon = pLon;
	}
	
	public double getLon(){
		return stopLon;
	}
	
	public void setLat(double pLat){
		stopLat = pLat;
	}
	
	public double getLat(){
		return stopLat;
	}
	
	public void setStopNom(String pNom){
		stopNom = pNom;
	}
	
	public String getStopNom(){
		return stopNom;
	}
	
	/**
	 * @return the stopId
	 */
	public String getStopId()
	{
		return stopId;
	}

	/**
	 * @param stopId the stopId to set
	 */
	public void setStopId(String stopId) 
	{
		this.stopId = stopId;
	}

	/**
	 * @return the numeroIncidentes
	 */
	public int getNumeroIncidentes()
	{
		numeroIncidentes = 0;
		updateNumeroIncidentes();
		return numeroIncidentes;
	}
	
	private void updateNumeroIncidentes(){
		StopUpdateVO.VOSchedule[] estados = update.getSchedules();
		for(StopUpdateVO.VOSchedule actual: estados){
			if(actual.getScheduleStatus().equalsIgnoreCase(StopUpdateVO.RETARDO)){
				numeroIncidentes ++;
			}
		}
	}
}
