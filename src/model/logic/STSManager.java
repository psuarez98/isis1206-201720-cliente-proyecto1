package model.logic;

import API.ISTSManager;
import model.data_structures.*;
import model.vo.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class STSManager implements ISTSManager
{

	//--------------------------------------------
	//Constantes
	//--------------------------------------------

	public static final String RUTA = "./data/";

	public static final String EXTEN = ".csv";

	public static final String EXTEN_JSON = ".json";

	public static final String JSON_BUS = "Bus_Service_";
	
	public static final String JSON_STOP = "Stops_Estim_Service_";

	//--------------------------------------------
	//Atributos
	//--------------------------------------------
	private IList<VOParada> paradas;

	private IList<VORuta> rutas;

	private IList<VOServicio> servicios;

	private IList<VOViaje> viajes;

	private IList<VOAgencia> agencias;

	private IQueue<BusUpdateVO> busesActuales;
	
	private IList<VOStopTimes> stopTimes;
	
	private IList <VORetardoViaje> retrasos = new DoubleLinkedList<VORetardoViaje>();
	
	private IList<VOSchedule> schedule;

	//--------------------------------------------
	//Metodos
	//--------------------------------------------

	//Inicializa las estructuras a usar
	@Override
	public void ITSInit()
	{
		paradas = new DoubleLinkedList<VOParada>();

		rutas = new DoubleLinkedList<VORuta>();

		servicios = new DoubleLinkedList<VOServicio>();

		viajes = new DoubleLinkedList<VOViaje>();

		agencias = new DoubleLinkedList<VOAgencia>();
		
		schedule = new DoubleLinkedList<VOSchedule>();

		/*
		 * - Shapes
		 */
	}

	//1C: Cargar la informacion estatica
	@Override
	public void ITScargarGTFS() 
	{
		ITSInit();

		try{
		cargarAgencias();
		System.out.println("=========================================================================");
		System.out.println("Se cargaron las agencias (Agency.csv)");
		System.out.println("=========================================================================");
		}
		catch(Exception e){
			//Se ignora y sigue
		}

		try{
		cargarParadas();
		System.out.println("=========================================================================");
		System.out.println("Se cargaron las paradas (Stops.csv)");
		System.out.println("=========================================================================");
		}
		catch(Exception e){
			//Se ignora y sigue
		}

		try{
		cargarViajes();
		System.out.println("=========================================================================");
		System.out.println("Se cargaron los viajes (Trips.csv)");
		System.out.println("=========================================================================");
		}
		catch(Exception e){
			//Se ignora y sigue
		}

		try{
		cargarRutas();
		System.out.println("=========================================================================");
		System.out.println("Se cargaron las rutas (Routes.csv)");
		System.out.println("=========================================================================");
		}
		catch(Exception e){
			//Se ignora y sigue
		}

		try{
		cargarServicios();
		System.out.println("=========================================================================");
		System.out.println("Se cargaron los servicios (Calendar.csv y Calendar_dates.csv)");
		System.out.println("=========================================================================");
		}
		catch(Exception e){
			//Se ignora y sigue
		}
		
		try{
			cargarTiemposDeParada();
			System.out.println("=========================================================================");
			System.out.println("Se cargaron los tiempos de parada (Stop_times.csv)");
			System.out.println("=========================================================================");			
		}
		catch(Exception e){
			//Se ignora y sigue
		}
	}

	//Metodos de carga para cada archivo segun el enunciado
	//cargarAgencias() carga Agency.csv
	//cargarParadas() carga Stops.csv
	//cargarViajes() carga Trips.csv
	//cargarRutas() carga Routes.csv
	//cargarServicios() carga Calendar.csv y Calendar_dates.csv
	//cargarTiemposDeParada() carga Stop_times.csv
	private void cargarAgencias(){
		String ruta = RUTA + "Agency" + EXTEN;
		try{
			File f = new File(ruta);

			if(!f.exists())
				throw new Exception("No se encontr� el archivo " + ruta);

			BufferedReader in = new BufferedReader(new FileReader(f));

			String line = in.readLine(); //Lee la linea de referencia
			line = in.readLine(); //Primera linea de contenido
			while(line != null){

				String[] data = line.split(",");

				data[0] = data[0].trim();
				data[1] = data[1].trim();

				VOAgencia aActual = new VOAgencia();
				aActual.setId(data[0]);
				aActual.setNom(data[1]);

				try{
					agencias.add(aActual);
				}
				catch(Exception e){
					//Se ignora y continua
				}

				line = in.readLine();

			}
			in.close();
		}
		catch(Exception e){
			System.out.println("================================================");
			System.out.println(e.getMessage());
			System.out.println("================================================");
		}
	}

	private void cargarParadas(){
		String ruta = RUTA + "Stops" + EXTEN;
		try{
			File f = new File(ruta);

			if(!f.exists())
				throw new Exception("No se encontr� el archivo " + ruta);

			BufferedReader in = new BufferedReader(new FileReader(f));

			String line = in.readLine(); //Lee la linea de referencia
			line = in.readLine(); //Lee la primera linea de contenido

			while(line != null){

				String[] data = line.split(",");

				for(String actual: data)
					actual = actual.trim();

				String id = data[0];
				String stopCode = data[1];
				String nom = data[2];
				String desc = data[3];
				long lat = Long.valueOf(data[4]);
				long lon = Long.valueOf(data[5]);

				VOParada pActual = new VOParada();
				
				pActual.setStopDesc(desc);
				pActual.setStopCode(stopCode);
				pActual.setLat(lat);
				pActual.setLon(lon);
				pActual.setStopId(id);
				pActual.setStopNom(nom);

				try{
					paradas.add(pActual);
				}
				catch(Exception e){
					//Se ignora y continua
				}

				line = in.readLine();

			}

			in.close();
		}
		catch(Exception e){
			System.out.println("================================================");
			System.out.println(e.getMessage());
			System.out.println("================================================");			
		}
	}

	private void cargarViajes(){
		String ruta = RUTA + "Trips" + EXTEN;
		try{
			File f = new File(ruta);

			if(!f.exists())
				throw new Exception("No se encontr� el archivo " + ruta);

			BufferedReader in = new BufferedReader(new FileReader(f));

			String line = in.readLine(); //Lee la linea de referencia
			line = in.readLine(); //Lee la primera linea de contenido

			while(line != null){

				String[] data = line.split(",");

				for(String actual: data)
					actual = actual.trim();

				String idRuta = data[0];
				String idServicio = data[1];
				String idViaje = data[2];

				VOViaje vActual = new VOViaje();

				vActual.setIdRuta(idRuta);
				vActual.setIdServicio(idServicio);
				vActual.setIdViaje(idViaje);

				try{
					viajes.add(vActual);
				}
				catch(Exception e){
					//Se ignora y continua
				}

				line = in.readLine();

			}

			in.close();
		}
		catch(Exception e){
			System.out.println("================================================");
			System.out.println(e.getMessage());
			System.out.println("================================================");			
		}
	}

	private void cargarRutas(){
		String ruta = RUTA + "Routes" + EXTEN;
		try{
			File f = new File(ruta);

			if(!f.exists())
				throw new Exception("No se encontr� el archivo " + ruta);

			BufferedReader in = new BufferedReader(new FileReader(f));

			String line = in.readLine(); //Lee la linea de referencia
			line = in.readLine(); //Lee la primera linea de contenido

			while(line != null){

				String[] data = line.split(",");

				for(String actual: data)
					actual = actual.trim();

				String idRuta = data[0];
				String idAgencia = data[1];
				String nom = data[2];

				VORuta rActual = new VORuta();

				rActual.setAgencyId(idAgencia);
				rActual.setIdRoute(idRuta);
				rActual.setShortName(nom);

				try{
					rutas.add(rActual);
				}
				catch(Exception e){
					//Se ignora y continua
				}

				line = in.readLine();

			}

			in.close();
		}
		catch(Exception e){
			System.out.println("================================================");
			System.out.println(e.getMessage());
			System.out.println("================================================");			
		}
	}

	private void cargarServicios(){
		String calendar = RUTA + "Routes" + EXTEN;
		try{
			File calendarF = new File(calendar);

			if(!calendarF.exists())
				throw new Exception("No se encontr� el archivo " + calendar);

			BufferedReader in = new BufferedReader(new FileReader(calendarF));

			String line = in.readLine(); //Lee la linea de referencia
			line = in.readLine(); //Lee la primera linea de contenido

			while(line != null){

				String[] data = line.split(",");

				int[] datos = new int[10];

				for(String actual: data){
					actual = actual.trim();
					datos[0] = Integer.parseInt(actual);
				}

				IList<String> dias = new DoubleLinkedList<String>();

				if(datos[1] == 1)
					dias.add("monday");
				if(datos[2] == 1)
					dias.add("tuesday");
				if(datos[3] == 1)
					dias.add("wednesday");
				if(datos[4] == 1)
					dias.add("thursday");
				if(datos[5] == 1)
					dias.add("friday");
				if(datos[6] == 1)
					dias.add("saturday");
				if(datos[7] == 1)
					dias.add("sunday");

				VOServicio sActual = new VOServicio();

				sActual.setDias(dias);
				sActual.setServiceId(data[0]);

				try{
					servicios.add(sActual);
				}
				catch(Exception e){
					//Se ignora y continua
				}

				line = in.readLine();

			}

			in.close();
			
			cargarExcepciones();
		}
		catch(Exception e){
			System.out.println("================================================");
			System.out.println(e.getMessage());
			System.out.println("================================================");			
		}
	}
	
	/**
	 * Se usa para cargar excepciones en los servicios usando Calendar_dates.csv
	 * @throws Exception
	 */
	private void cargarExcepciones() throws Exception{
		String ruta = RUTA + "Calendar_dates" + EXTEN;
		BufferedReader in = new BufferedReader(new FileReader(new File(ruta)));
		String line = in.readLine(); //Primera linea es de referencia
		line = in.readLine(); //Primera linea de contenido
		while(line != null){
			
			String[] data = line.split(",");
			
			for(String actual: data)
				actual = actual.trim();
			
			String id = data[0];
			String fecha = data[1];
			
			for(int i=0; i<servicios.getSize(); i++){
				if(servicios.getElement(i).getServiceId().equalsIgnoreCase(id))
					servicios.getElement(i).setFechaExcepcion(fecha);
			}
			
			line = in.readLine();
		}
		
		in.close();
	}
	
	private void cargarTiemposDeParada(){
		stopTimes= new DoubleLinkedList<VOStopTimes>();
		FileReader lector;
		BufferedReader in;
		String ruta = RUTA + "Stop_times" + EXTEN;
		String linea = null ;
		//Calendar hora= Calendar.getInstance();
		try {

			lector = new FileReader(new File(ruta));
			in = new BufferedReader(lector);
			linea = in.readLine(); //Linea de referencia
			linea = in.readLine(); //Primera linea de contenido


			while(linea != null)
			{

				String[] datos = linea.split(",");
				int trip_id = -1;
				try{
					trip_id = Integer.parseInt( datos[0].trim());
				}
				catch(Exception e){

				}

				String[] arrival_time =(datos[1].split(":"));
				int horaA= Integer.parseInt(arrival_time[0].trim());
				int minA= Integer.parseInt(arrival_time[1].trim());
				int segA= Integer.parseInt(arrival_time[2].trim());
				//hora.set(Calendar.HOUR, Integer.parseInt(arrival_time[0]));
				//hora.set(Calendar.MINUTE, Integer.parseInt(arrival_time[1]));
				//hora.set(Calendar.SECOND, Integer.parseInt(arrival_time[2]));

				String[] departure_time =(datos[2].split(":"));
				int horaD= Integer.parseInt(departure_time[0].trim());
				int minD= Integer.parseInt(departure_time[1].trim());
				int segD= Integer.parseInt(departure_time[2].trim());
				//hora.set(Calendar.HOUR, Integer.parseInt(departure_time[0]));
				//hora.set(Calendar.MINUTE, Integer.parseInt(departure_time[1]));
				//hora.set(Calendar.SECOND, Integer.parseInt(departure_time[2]));

				int stop_id = -1;
				try{
					stop_id = Integer.parseInt(datos[3]);
				}
				catch(Exception e){

				}

				int stop_sequence = -1;

				try{
					stop_sequence = Integer.parseInt( datos[4].trim());
				}
				catch(Exception e){

				}
				String stop_headsign = "";
				try{
					stop_headsign = datos[5];
				}
				catch(Exception e){

				}
				//int pickup_type= Integer.parseInt( datos[6]);
				int drop_off_type= -1;
				try{
					drop_off_type = Integer.parseInt( datos[7].trim());
				}
				catch(Exception e){

				}

				double shape_dist_traveled= -1;
				try{
					shape_dist_traveled = Double.parseDouble(datos[8].trim());
				}
				catch(Exception e){

				}

				//Se crea un objeto a partir del archivo para ser agregado a la lista
				VOStopTimes temp = new VOStopTimes(trip_id, horaA, minA, segA, horaD, minD, segD, stop_id, stop_sequence, stop_headsign, drop_off_type, shape_dist_traveled);
				stopTimes.add(temp);


				linea = in.readLine();
			}
			in.close();
			lector.close();
		}

		catch (Exception e) 
		{
			System.out.println("================================================");
			System.out.println(e.getMessage());
			System.out.println("================================================");	
		}
	}

	//1C: Cargar la informacion de un bus para una fecha dada
	@Override
	public void ITScargarTR(String fecha) 
	{
		String ruta = RUTA + JSON_BUS + fecha + EXTEN_JSON;
		try{
			File f = new File(ruta);
			if(!f.exists())
				throw new Exception("No se encontro el archivo " + ruta);

			readBusUpdate(f);
			System.out.println("================================================");
			System.out.println("Se carg� el archivo " + JSON_BUS + fecha + EXTEN_JSON);
			System.out.println("================================================");	
		}
		catch(Exception e){
			System.out.println("================================================");
			System.out.println(e.getMessage());
			System.out.println("================================================");	
		}
	}
	
	private void readBusUpdate(File rtFile) throws Exception 
	{
		Gson gson = new Gson(); 

		busesActuales = new Queue<BusUpdateVO>();

		BufferedReader br = new BufferedReader(new FileReader(rtFile));
		JsonParser parser = new JsonParser();
		JsonArray array = parser.parse(br).getAsJsonArray();

		for(int i = 0 ; i < array.size() ; i++) 
		{
			JsonElement yourJson = array.get(i);
			//String result1 = yourJson.toString();
			JsonObject objeto = yourJson.getAsJsonObject();
			BusUpdateVO newUpdates = gson.fromJson(objeto.toString(), BusUpdateVO.class);
			busesActuales.enqueue(newUpdates);
		}
		
		br.close();
	}
	
	private void readStopUpdate(String fecha, VOParada agregar) throws Exception{
		
		Gson gson = new Gson();
		
		
		String ruta = RUTA + JSON_STOP + agregar.getStopCode() + "_" + fecha + EXTEN_JSON;
		
		File rtFile = new File(ruta);
		
		BufferedReader in = new BufferedReader(new FileReader(rtFile));
		JsonParser parser = new JsonParser();
		JsonArray array = parser.parse(in).getAsJsonArray();
		
		for(int i=0; i<array.size(); i++){
			JsonElement yourJson = array.get(i);
			//String result1 = yourJson.toString();
			JsonObject objeto = yourJson.getAsJsonObject();
			StopUpdateVO newUpdate = gson.fromJson(objeto.toString(), StopUpdateVO.class);
			agregar.setUpdate(newUpdate);
		}
		
		in.close();
	}

	/**
	 * @author pa.suarezm
	 */
	//1A: Dar rutas por nombre de empresa en fecha dada
	@Override
	public IList<VORuta> ITSrutasPorEmpresa(String nombreEmpresa, String fecha)
	{
		/* 1. Buscar el id de la empresa asociado al nombreEmpresa 
		 * 2. Agregar todas las rutas que correspondan al id encontrado en 1.
		 * 3. Verificar si en la fecha dada, estaban funcionando las rutas encontradas
		 * 4. Ordenar las rutas encontradas por su id
		 * 5. Retornar las rutas que quedaron en el punto 4.
		 */

		//1.
		String idAgencia = null;
		for(VOAgencia actual: agencias){
			if(actual.getId().equalsIgnoreCase(nombreEmpresa)||actual.getNom().equalsIgnoreCase(nombreEmpresa)){
				idAgencia = actual.getId();
				break;
			}
		}

		if(idAgencia == null){
			System.out.println("=================================================================");
			System.out.println("No se encontro ninguna agencia con el nombre ingresado");
			System.out.println("=================================================================");	
			return new DoubleLinkedList<VORuta>();
		}

		//2.
		IList<VORuta> rutasAsociadas = new DoubleLinkedList<VORuta>();
		for(VORuta actual: rutas){
			if(actual.getAgencyId().equalsIgnoreCase(idAgencia)){
				try{
					rutasAsociadas.add(actual);
				}
				catch(Exception e){
					//Se ignora y continua
				}
			}
		}

		//3.
		String dia = "";

		int diaDeLaSemana = darDiaDeLaSemana(fecha);

		if(diaDeLaSemana == 1)
			dia = "sunday";
		else if(diaDeLaSemana == 2)
			dia = "monday";
		else if(diaDeLaSemana == 3)
			dia = "tuesday";
		else if(diaDeLaSemana == 4)
			dia = "wednesday";
		else if(diaDeLaSemana == 5)
			dia = "thursday";
		else if(diaDeLaSemana == 6)
			dia = "friday";
		else if(diaDeLaSemana == 7)
			dia = "saturday";

		IList<VORuta> respuesta = new DoubleLinkedList<VORuta>();

		for(VORuta actual: rutasAsociadas){
			if(funcionaEnElDia(dia, actual, fecha)){
				try{
					respuesta.add(actual);
				}
				catch(Exception e){
					//Se ignora y continua
				}
			}
		}

		//4.
		respuesta = ordenarPorIdRuta(respuesta);

		//5.
		return respuesta;
	}

	/**
	 * Ordena la lista usando una especie de SelectionSort (Se podr�a hacer m�s eficiente con otro algoritmo, pero
	 * este es el m�todo m�s f�cil y r�pido de hacer (Tambi�n toca hacerlo en O(n) en t�rminos de memoria
	 * por problemas con la implementaci�n de DoubleLinkedList))
	 * @param lista La lista para ordenar por el Id de ruta ascendentemente
	 * @author pa.suarezm
	 */
	private IList<VORuta> ordenarPorIdRuta(IList<VORuta> lista){

		IList<VORuta> respuesta = new DoubleLinkedList<VORuta>();

		for(int i=0; i<lista.getSize(); i++){
			VORuta menor = lista.getElement(0);
			int indexMenor = 0;
			for(int j=0; j<lista.getSize(); j++){
				VORuta actual = lista.getElement(j);
				if(menor.getIdRoute().compareTo(actual.getIdRoute()) > 0){
					menor = actual;
					indexMenor = j;
				}
			}
			try{
				respuesta.add(menor);
				lista.deleteAtK(indexMenor);
			}
			catch(Exception e){
				//Se ignora y sigue
			}
		}

		return respuesta;
	}

	/**
	 * Dado un d�a de la semana y una ruta, verifica si dicha ruta funciona en el dia dado
	 * @param pDia Dia de la semana en ingles y minusculas
	 * @param rBuscado Ruta a verificar
	 * @param pFecha fecha exacta en la que se quiere averiguar sin funciona o no
	 * @return boolean. true si s� funciona en el d�a pDia o false en caso contrario
	 * @author pa.suarezm
	 */
	private boolean funcionaEnElDia(String pDia, VORuta rBuscado, String pFecha){
		boolean respuesta = false;

		/* 1. Usando el id de la ruta busca a cual servicio corresponde
		 * 2. Con el al encontrar el id del servicio busca el objeto al que corresponde en servicios
		 * 3. Busca dentro del objeto qu� d�as de la semana funciona
		 * 4. Si alguno de esos dias es igual a pDia, retorna true
		 */

		String idRuta = rBuscado.getIdRoute();
		//1.
		String idServicio = "";

		boolean encontro = false;
		for(int i=0; i<viajes.getSize() && !encontro; i++){
			VOViaje actual = viajes.getElement(i);
			if(actual.getIdRuta().equalsIgnoreCase(idRuta)){
				idServicio = actual.getIdServicio();
				encontro = true;
			}
		}

		//2.
		VOServicio buscado = null;

		encontro = false;
		for(int i=0; i<servicios.getSize() && !encontro; i++){
			VOServicio actual = servicios.getElement(i);
			if(actual.getServiceId().equalsIgnoreCase(idServicio)){
				buscado = actual;
				encontro = true;
			}
		}

		//3.
		for(String semana: buscado.getDias()){
			if(semana.equalsIgnoreCase(pDia) && !buscado.getFechaExcepcion().equalsIgnoreCase(pFecha)){
				respuesta = true;
				break;
			}
		}

		//4.
		return respuesta;
	}

	/**
	 * Dada una fecha en formato String yyyyMMdd devuelve un entero entre 1 y 7 que describe a qu� d�a de la semana corresponde
	 * @param pDate Entra una fecha como una String de la forma yyyyMMdd
	 * @return El dia de la semana que empieza en domingo(1) y termina en sabado(7)
	 * @author pa.suarezm
	 */
	private int darDiaDeLaSemana(String pDate){
		Calendar c = Calendar.getInstance();
		SimpleDateFormat source = new SimpleDateFormat("yyyyMMdd");
		Date d = new Date();
		try{
			d = source.parse(pDate);
		}
		catch(Exception e){
			e.printStackTrace();
		}

		c.setTime(d);

		int day = c.get(Calendar.DAY_OF_WEEK);

		return day;
	}

	//2A: Dar viajes retrasados para una ruta en una fecha dada
	@Override
	public IList<VOViaje> ITSviajesRetrasadosRuta(String idRuta, String fecha) 
	{
		//TODO
		return null;
	}

	/**
	 * Dada una fecha, retorna todas las paradas para las que hubieron retardos en la fecha dada
	 * @param fecha La fecha para la cual se buscan las paradas con incidentes
	 * @return Lista ordenada por numero de incidentes con todas las paradas en las que hubo retardos
	 * @author pa.suarezm
	 */
	//3A: Dar paradas que tuvieron retrasos en una fecha dada
	@Override
	public IList<VOParada> ITSparadasRetrasadasFecha(String fecha) 
	{
		/* 1. Inicializar la lista que al final se va a retornar
		 * Teniendo una fecha, solo falta recorrer todas las actualizaciones para todos los stopcodes
		 * 2. Recorrer cada parada, y si hubo un retardo en la fecha indicada se agrega a la lista
		 * 		2.1 Se debe cargar primero la actualziacion de la parada actual
		 * 		2.2 Mirar si la parada actual tiene retardos, si los tiene agregarlos a las lista 1.
		 * 3. Ordenar la lista a retornar
		 */
		
		//1.
		IList<VOParada> paradasRetrasadasFecha = new DoubleLinkedList<VOParada>();
		
		//2
		for(int i=0; i<paradas.getSize(); i++){
			VOParada actual = paradas.getElement(i);
			try{
				//2.1
				readStopUpdate(fecha, actual);
				
				//2.2
				int retardos = actual.getNumeroIncidentes();
				if(retardos != 0)
					paradasRetrasadasFecha.add(actual);
			}
			catch(Exception e){
				//Se ignora y continua
			}
		}
		
		//3
		paradasRetrasadasFecha = ordenarPorIncidentes(paradasRetrasadasFecha);
		
		return paradasRetrasadasFecha;
	}
	
	/**
	 * Ordena la lista usando una especie de SelectionSort (Se podr�a hacer m�s eficiente con otro algoritmo, pero
	 * este es el m�todo m�s f�cil y r�pido de hacer (Tambi�n toca hacerlo en O(n) en t�rminos de memoria
	 * por problemas con la implementaci�n de DoubleLinkedList))
	 * @param lista La lista para ordenar por el numero de incidentes en la parada
	 * @author pa.suarezm
	 */
	private IList<VOParada> ordenarPorIncidentes(IList<VOParada> param){
		IList<VOParada> resultado = new DoubleLinkedList<VOParada>();
		
		for(int i=0; i<param.getSize(); i++){
			VOParada menor = param.getElement(0);
			int indexMenor = 0;
			for(int j=0; j<param.getSize(); j++){
				VOParada actual = param.getElement(j);
				if(actual.getNumeroIncidentes()<menor.getNumeroIncidentes()){
					menor = actual;
					indexMenor = j;
				}
			}
			try{
				resultado.add(menor);
				param.deleteAtK(indexMenor);
			}
			catch(Exception e){
				//Se ignora y sigue
			}
		}
		
		return resultado;
	}

	//4A: Dar transbordos posibles y las paradas asociadas
	@Override
	public IList<VOTransfer> ITStransbordosRuta(String idRuta, String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

	//5A: Dar rutas asociadas a las paradas en una fecha y hora dadas
	@Override
	public VOPlan ITSrutasPlanUtilizacion(IList<String> idsDeParadas, String fecha, String horaInicio, String horaFin) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @author cv.trujillo
	 */
	//1B
	@Override
	public IList<VORuta> ITSrutasPorEmpresaParadas(String nombreEmpresa, String fecha)
	{

		IList<VORuta> rutasFinal = new DoubleLinkedList<VORuta>();

		//El dia de la semana empieza en domingo(1) y termina en sabado(7)
		int diaFecha = darDiaDeLaSemana(fecha);


		//1. Conseguir todos los servicios que funcionaban ese d�a
		IList<Integer> servicesId = new DoubleLinkedList<Integer>();
		for(int i=0; i<servicios.getSize(); i++){
			VOServicio actual = servicios.getElement(i);
			//TODO verficar el dia y agregar si s�
		}

		//2. Verificar si la fecha tiene excepciones
		boolean tieneExcepcion = false;
		int fechaEntera = Integer.parseInt(fecha);
		for(int i=0; i<servicios.getSize() && !tieneExcepcion; i++){
			if(fechaEntera == Integer.parseInt(servicios.getElement(i).getFechaExcepcion())){
				tieneExcepcion = true;
			}
		}


		//3. Conseguir las rutas asociadas al servicio usando trips.txt y verificar si tienen excepciones
		IList<Integer> routesId = new DoubleLinkedList<Integer>();
		for(int i=0; i<viajes.getSize(); i++){
			VOViaje actual = viajes.getElement(i);
			for(int j=0; j<servicesId.getSize(); j++){
				int idActual = servicesId.getElement(j);
				if(idActual == Integer.parseInt(actual.getIdServicio())){
					if(tieneExcepcion){
						boolean encontrado = false;
						
						//TODO
						for(int k=0; k<servicios.getSize(); k++){
							if(servicios.getElement(k).getServiceId().equalsIgnoreCase(String.valueOf(idActual)))
								encontrado = true;
						}
						if(!encontrado)
							routesId.add(Integer.parseInt(actual.getIdRuta()));
					}
					else
						routesId.add(Integer.parseInt(actual.getIdRuta()));
				}
			}
		}

		//4. Agregar objetos VORoute
		IList<VORuta> rutas = new DoubleLinkedList<VORuta>();
		for(int i=0; i<rutas.getSize(); i++){
			VORuta actual = rutas.getElement(i);
			for(int j=0; j<routesId.getSize(); j++){
				int idActual = routesId.getElement(j);
				if(Integer.parseInt(actual.getIdRoute()) == idActual){
					rutas.add(actual);
				}
			}
		}

		//5. Ordenar la lista por paradas de ruta
		for(int i=0; i<rutas.getSize(); i++){
			VORuta menor = rutas.getElement(i);
			int indexMenor = i;
			for(int j=0; j<rutas.getSize(); j++){
				DoubleLinkedList paradas= paradasDeRutas(menor.getShortName());
				for (int k = 0; k < paradas.getSize(); k++)
				{
					VOParada lasParadaAcutal= (VOParada) paradas.getElement(k);
					VOParada lasParadasMenor= (VOParada) paradas.getElement(indexMenor);

					int cantidadParadasActuales= Integer.parseInt(lasParadaAcutal.getStopDesc());
					int cantParadasMenor= Integer.parseInt(lasParadasMenor.getStopDesc());

					if(cantParadasMenor> cantidadParadasActuales){
						menor = rutas.getElement(j);
						indexMenor = j;
					}
				}
			}
			rutasFinal.add(menor);
			rutas.deleteAtK(indexMenor);
		}

		return rutasFinal;
	}
	
	/**
	 * @author cv.trujillo
	 */
	private DoubleLinkedList<String> paradasDeRutas(String routeName) {

		String stopId ="";

		if(stopId.equalsIgnoreCase("error")){
			System.out.println("error");
			return null;
		}

		for(int i=0; i<paradas.getSize() ; i++){

			if(routeName.equalsIgnoreCase(((VOParada) paradas.getElement(i)).getStopNom())){
				stopId = String.valueOf(((VOParada) paradas.getElement(i)).getStopId());
			}
		}
		
		//TODO Recorrer Stop_times.csv
		DoubleLinkedList<String> tripsId = new DoubleLinkedList<String>();
		for(VOStopTimes actual: stopTimes){
			int stopIdP =Integer.parseInt(stopId);
			if(!tripsId.yaExiste(actual.getTripId()) && Integer.parseInt(actual.getStopId())==stopIdP){
				String idStops = actual.getStopId();
				String idStopsS = ""+idStops;
				tripsId.add(idStopsS);
			}
		}
		
		return tripsId;
	}

	/**
	 * @author cv.trujillo
	 */
	//2B
	@Override
	public IList<VOViaje> ITSviajesRetrasoTotalRuta(String idRuta, String fecha)
	{
		IList <VOViaje> losViajes = new DoubleLinkedList();
		IList retardos= new DoubleLinkedList();


		for (int i = 0; i < rutas.getSize(); i++)
		{
			VORuta nombresdeRUTAS= rutas.getElement(i);
			String nombredeEsaRuta= nombresdeRUTAS.getIdRoute();

			if(nombredeEsaRuta.equals(idRuta))
			{
				IList listaDeParadasRutas= paradasDeRutas(nombredeEsaRuta); 
				for (int j = 0; j < listaDeParadasRutas.getSize(); j++) 
				{
					for (int j2 = 0; j2 < stopTimes.getSize(); j2++) 
					{
						VOStopTimes paradas= stopTimes.getElement(j2);		
						int horaLlegada= paradas.getArrival_time();

						VOParada paradasElementos= (VOParada) listaDeParadasRutas.getElement(j);
						Double darLatitud= paradasElementos.getLat();
						Double darLingitud= paradasElementos.getLon();
						for (int k = 0; k <schedule.getSize(); k++) {
							VOSchedule horarios= schedule.getElement(k);
							Date llegada= horarios.getExpectedLeaveTime();
							if(horaLlegada != llegada.getDate())
							{
								retardos.add(nombredeEsaRuta);
							}
						}

					}
				}

			}

		}

		//ordenar id
		for(int i=0; i<retardos.getSize(); i++){
			VOViaje menor = (VOViaje) retardos.getElement(i);
			int indexMenor = i;
			for(int j=i; j<retardos.getSize(); j++){

				int paradasMenor = Integer.parseInt(menor.getIdViaje());
				int paradasActual = Integer.parseInt(((VOStopTimes)retardos.getElement(i)).getTripId());
				if(paradasMenor > paradasActual){
					menor = (VOViaje) retardos.getElement(j);
					indexMenor = j;
				}
			}
			retardos.add(menor);
			retardos.deleteAtK(indexMenor);
		}


		//ordenar seg�n los retardos
		for(int i=0; i<retardos.getSize(); i++){
			VOViaje menor = (VOViaje) retardos.getElement(i);
			int indexMenor = i;
			for(int j=i; j<retardos.getSize(); j++){

				int retardosMenor = Integer.parseInt(menor.getIdViaje());

				int retardosActual =Integer.parseInt(((VOStopTimes)retardos.getElement(i)).getTripId());
				if(retardosMenor < retardosActual){
					menor = (VOViaje) retardos.getElement(j);
					indexMenor = j;
				}
			}
			retardos.add(menor);
			//porque al est�r agregando lo necesito, lo a�ado y lo elimino de otro lado...Estoy agrgando no intercambiando
			retardos.deleteAtK(indexMenor);
		}

		return losViajes;
	}

	/**
	 * @author cv.trujillo
	 */
	//3B
	@Override
	public IList<VORangoHora> ITSretardoHoraRuta(String idRuta, String Fecha)
	{
		IList<VORangoHora> rangos = new DoubleLinkedList<>();
		IList<String> fechas=new DoubleLinkedList<>();
		IList<VOViaje> viajesRetrasados=new DoubleLinkedList<>();
		IList<VOServicio> services=new DoubleLinkedList<>();
		int tempMayor=0;

		VORuta routeA = getRoute(idRuta);

		for (int i = 0; i < servicios.getSize(); i++) {
			
			
			
			/*String[] cont=(( calendar.getElement(i)).toString()).split(";");
			if(cont[1].equalsIgnoreCase(fecha))
				fechas.add((calendar.getElement(i)).toString());*/
		}
		for (int i = 0; i < viajes.getSize(); i++) {
			if((""+ viajes.getElement(i).getIdRuta()).equalsIgnoreCase(idRuta)) {
				for (int j = 0; j < fechas.getSize(); j++) {
					String [ ] cont=fechas.getElement(j).split(";");
					if(cont[0].equals((""+ viajes.getElement(i).getIdServicio())))
					{
						viajesRetrasados.add(viajes.getElement(i));
						break;
					}
				}
			}
		}

		for (int i = 0; i < viajes.getSize(); i++) {
			for (int j = 0; j < viajesRetrasados.getSize(); j++) {
				if(((Integer.parseInt(viajes.getElement(i).getIdViaje()))==(Integer.parseInt((viajesRetrasados.getElement(j)).getIdViaje())))) {
					services.add( servicios.getElement(i));
					break;
				}
			}
		}

		IList<VORuta> listaServiciosConRetardo = getListaServicesRetardos();


		for (int i = 0; i < services.getSize(); i++) {
			retrasos=getListaServicesRetardos().getElement(i).getSchedules();
			//			int NR= retrasos.size();

			for (int j = 0; j < retrasos.getSize(); j++) {
				int contador=0;
				String status= retrasos.getElement(j).getScheduleStatus();
				String leaveTime= (retrasos.getElement(j).getExpectedLeaveTime()).toString();

				if(status.equalsIgnoreCase("-"))
				{
					for (int k = j+1; k < retrasos.getSize(); k++) {
						if(retrasos.getElement(k).getExpectedLeaveTime().equals(leaveTime)&&
								retrasos.getElement(k).getScheduleStatus().equalsIgnoreCase(status)) {
							contador ++;
						}
						if(contador>=tempMayor){
							tempMayor=contador;
							String dateA = (retrasos.getElement(k).getLastUpdate()).toString();
							int lastUpdateA = Integer.parseInt(dateA);
							rangos.add(new VORangoHora(Integer.parseInt(leaveTime), lastUpdateA));
						}
					}
				}
			}
		}

		return rangos;
	}
	
	/**
	 * @author cv.trujillo
	 */
	private VORuta getRoute(String routeId)
	{
		VORuta routeA=null;
		Iterator it =rutas.iterator();
		while(it.hasNext())
		{
			String routeIDN = routeId;
			VORuta actual = (VORuta)it.next();
			if(actual.getIdRoute().equals(routeIDN))
			{
				routeA=actual;
			}
		}	
		return routeA;
	}
	
	/**
	 * @author cv.trujillo
	 */
	private IList<VORuta> getListaServicesRetardos() {
		IList<VORuta> retardos=new DoubleLinkedList<>();


		for (int i = 0; i < retrasos.getSize(); i++) {
			VORetardoViaje retardoActual = retrasos.getElement(i);
			for (int j = 0; j < viajes.getSize(); j++) {
				VOViaje tripA = viajes.getElement(i);
				if(retardoActual.getViajeid()== Integer.parseInt(tripA.getIdViaje() )&& retardoActual.getTiempoRetardo()!=0)
				{

					retardos.add(getRoute(""+tripA.getIdViaje()));
				}
			}

		}

		return retardos;
	}

	//4B
	@Override
	public IList<VOViaje> ITSbuscarViajesParadas(String idOrigen, String idDestino, String fecha, String horaInicio,
			String horaFin) 
	{
		IList <VOViaje> viajesL= new DoubleLinkedList();

		//1. Buscar los viajes
		for (int i = 0; i < viajes.getSize(); i++)
		{
			calendarDates.getElement(i).toString().split(";");

			for (int j = 0; j < stopTimes.getSize(); j++) 
			{
				if((stopTimes.getElement(i).getTripId().equals(trips.getElement(i).getId()))&& (stopTimes.getElement(i).getArrival_time()== stopTimes.getElement(i).getArrival_time()) && (stopTimes.getElement(i).getArrival_time()== stopTimes.getElement(i).getDeparture_time()))
				{
					for (int j2 = 0; j2 < calendarDates.getSize(); j2++)
					{
						String [] calendar= calendarDates.getElement(j2).toString().split(":");
						if(calendar[0].equals(viajes.getElement(i).getIdServicio()))
						{
							IList <VOTransfer> actualTransfer= new DoubleLinkedList<>();
							for (int k = 0; i < actualTransfer.getSize(); k++) 
							{
								VOTransfer actual= actualTransfer.getElement(k);
								String cont[]=actual.toString().split(";");
								if(cont[1].equals(stopTimes.getElement(j+1).getStopId()))
								{
									viajes.add(viajes.getElement(i));
								}

							}
						}
					}
				}
			}


		}


		return viajes;
	}

	@Override
	public VORuta ITSrutaMenorRetardo(String fecha) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @author cv.trujillo
	 */
	//2C
	@Override
	public IList<VOServicio> ITSserviciosMayorDistancia(String fecha)
	{
		IList <CalendarDateVO> listasAns= new DoubleLinkedList<>();
		int mayor=0;
		for (int i = 0; i < calendarDates.getSize(); i++) {
			CalendarDateVO arreglo= calendarDates.getElement(i);
			int laFecha= arreglo.getDate();
			if(laFecha== Integer.parseInt(fecha))
			{
				for (int j = 0; j < servicios.getSize(); j++)
				{
					listasAns.add(servicios.getElement(j).getaCalendar());	
				}
			}
		}
		for (int k = 0; k < listasAns.getSize(); k++) {
			CalendarDateVO actualServ= listasAns.getElement(k);
			CalendarDateVO asiguienteServ= listasAns.getElement(k+1);
			int distancia1= actualServ.getService().getDistanciaRecorrida();
			int distancia2= asiguienteServ.getService().getDistanciaRecorrida();

			if(distancia1>mayor)
			{
				listasAns.add(actualServ);

			}

		}
		return listasAns;
	}

	@Override
	public IList<VORetardo> ITSretardosViaje(String fecha, String idViaje) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOParada> ITSparadasCompartidas(String fecha)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
