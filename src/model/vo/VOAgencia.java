package model.vo;

public class VOAgencia {
	
	private String id;
	
	private String nom;
	
	public String getId(){
		return id;
	}
	
	public String getNom(){
		return nom;
	}
	
	public void setId(String pId){
		id = pId;
	}
	
	public void setNom(String pNom){
		nom = pNom;
	}
	
}
