package model.vo;

import com.google.gson.annotations.SerializedName;

public class StopUpdateVO {
	
	//--------------------------------------
	//Constantes
	//--------------------------------------
	public static final String RETARDO = "-";
	public static final String A_TIEMPO = "*";
	public static final String ADELANTADO = "+";

	//--------------------------------------
	//Atributos
	//--------------------------------------
	
	@SerializedName("RouteNo") private String RouteNo;
	@SerializedName("RouteName") private String RouteName;
	@SerializedName("Direction") private String Direction;
	@SerializedName("RouteMap") private UrlRoute RouteMap;
	@SerializedName("Schedules") private VOSchedule[] Schedules;
	
	
	//---------------------------------------
	//Clases anidadas
	//---------------------------------------
	public class VOSchedule{
		
		//Atributos
		@SerializedName("") private String Pattern;
		@SerializedName("") private String Destination;
		@SerializedName("") private String ExpectedLeaveTime;
		@SerializedName("") private String ExpectedCountdown;
		@SerializedName("") private String ScheduleStatus;
		@SerializedName("") private boolean CancelledTrip;
		@SerializedName("") private boolean CancelledStop;
		@SerializedName("") private boolean AddedTrip;
		@SerializedName("") private boolean AddedStop;
		@SerializedName("") private String LastUpdate;
		
		//Constructor
		public VOSchedule(String pPattern, String pDest, String pELT, String pEC, String SS, boolean pCT, boolean pCS,
				boolean pAT, boolean pAS, String pLU){
			Pattern = pPattern;
			Destination = pDest;
			ExpectedLeaveTime = pELT;
			ExpectedCountdown = pEC;
			ScheduleStatus = SS;
			CancelledTrip = pCT;
			CancelledStop = pCS;
			AddedTrip = pAT;
			AddedStop = pAS;
			LastUpdate = pLU;
		}
		
		//Metodos
		public String getScheduleStatus(){
			return ScheduleStatus;
		}
		
	}
	
	
	public class UrlRoute{
		@SerializedName("Href") private String Href;
		public UrlRoute(String href){	
				Href = href;
		}
	}
	
	//----------------------------------------
	//Constructor
	//----------------------------------------
	public StopUpdateVO(String pRouteNo, String pRouteName, String pDirection, UrlRoute pRouteMap, VOSchedule[] pSchedules){
		RouteNo = pRouteNo;
		RouteName = pRouteName;
		Direction = pDirection;
		RouteMap = pRouteMap;
		Schedules = pSchedules;
	}
	
	//----------------------------------------
	//Metodos
	//----------------------------------------
	public VOSchedule[] getSchedules(){
		return Schedules;
	}
}
