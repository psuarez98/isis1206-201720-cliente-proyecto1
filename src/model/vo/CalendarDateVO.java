package model.vo;

public class CalendarDateVO {
	
	//-----------------------------------------
	//Atributos
	//-----------------------------------------
	
	private int serviceId;
	private int date;
	private int exceptionType;
	private VOServicio service;
	
	//-----------------------------------------
	//Constructor
	//-----------------------------------------
	
	public CalendarDateVO(int pId, int pDate, int pET){
		serviceId = pId;
		date = pDate;
		exceptionType = pET;
	}
	
	//------------------------------------------
	//Metodos
	//------------------------------------------
	
	public int getServiceId() {
		return serviceId;
	}

	public int getDate() {
		return date;
	}

	public int getExceptionType() {
		return exceptionType;
	}
	
	public VOServicio getService()
	{
		return service;
	}
	
}
