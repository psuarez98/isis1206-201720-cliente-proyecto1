package model.vo;

import model.data_structures.*;

public class VOServicio 
{
	
	private IList <VORetardoViaje> listaDeRetardos;
	
	/**
	 * Modela el id del servicio
	 */
     private String serviceId;

     /**
      * Modela la sistancia recorrida del servicio dado una fecha (req 2C)
      */
     private int distanciaRecorrida;
     
     /**
      * Modela los d�as que funciona de la semana que funciona el servicio (req 1A)
      */
     private IList<String> diasHabiles;
     
     private String fechaExcepcion;
     
     public String getFechaExcepcion(){
    	 return fechaExcepcion;
     }
     
     public void setFechaExcepcion(String pFecha){
    	 fechaExcepcion = pFecha;
     }
     
     public IList<String> getDias(){
    	 return diasHabiles;
     }
     
     public void setDias(IList<String> pDias){
    	 diasHabiles = pDias;
     }
     
	/**
	 * @return the serviceId
	 */
	public String getServiceId() 
	{
		return serviceId;
	}

	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(String serviceId) 
	{
		this.serviceId = serviceId;
	}

	/**
	 * @return the distanciaRecorrida
	 */
	public int getDistanciaRecorrida()
	{
		return distanciaRecorrida;
	}

	/**
	 * @param distanciaRecorrida the distanciaRecorrida to set
	 */
	public void setDistanciaRecorrida(int distanciaRecorrida)
	{
		this.distanciaRecorrida = distanciaRecorrida;
	}
	
	public IList <VORetardoViaje> getListaViajeRetardado()
	{
		return listaDeRetardos;
	}
}
