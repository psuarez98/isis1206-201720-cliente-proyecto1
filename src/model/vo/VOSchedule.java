package model.vo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VOSchedule {
	private String pattern ;
	private String destination ;
	private Date expectedLeaveTime  ;
	private int expectedCountdown ;
	private String scheduleStatus ;
	private boolean cancelledTrip ;
	private boolean cancelledStop ;
	private boolean addedTrip; 
	private boolean addedStop ;
	private Date lastUpdate  ;
	
	
	public VOSchedule(String pPattern, String pDestination, Date pExpectedLeaveTime, int pExpectedCountDown, String pScheduleStatus,
					  boolean pCancelledTrip, boolean pCancelledStop, boolean pAddedTrip, boolean pAddedStop, Date pLastUpdate ) {
		pattern=pPattern;
		destination=pDestination;
		expectedLeaveTime=pExpectedLeaveTime;
		expectedCountdown=pExpectedCountDown;
		scheduleStatus=pScheduleStatus;
		cancelledTrip=pCancelledTrip;
		cancelledStop=pCancelledStop;
		addedTrip=pAddedTrip;
		addedStop=pAddedStop;
		lastUpdate=pLastUpdate;
	}
	
	
	
	public String getPattern() {
		return pattern;
	}
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public Date getExpectedLeaveTime() {
		return expectedLeaveTime;
	}
	public void setExpectedLeaveTime(Date expectedLeaveTime) {
		this.expectedLeaveTime = expectedLeaveTime;
	}
	public int getExpectedCountdown() {
		return expectedCountdown;
	}
	public void setExpectedCountdown(int expectedCountdown) {
		this.expectedCountdown = expectedCountdown;
	}
	public String getScheduleStatus() {
		return scheduleStatus;
	}
	public void setScheduleStatus(String scheduleStatus) {
		this.scheduleStatus = scheduleStatus;
	}
	public boolean isCancelledTrip() {
		return cancelledTrip;
	}
	public void setCancelledTrip(boolean cancelledTrip) {
		this.cancelledTrip = cancelledTrip;
	}
	public boolean isCancelledStop() {
		return cancelledStop;
	}
	public void setCancelledStop(boolean cancelledStop) {
		this.cancelledStop = cancelledStop;
	}
	public boolean isAddedTrip() {
		return addedTrip;
	}
	public void setAddedTrip(boolean addedTrip) {
		this.addedTrip = addedTrip;
	}
	public boolean isAddedStop() {
		return addedStop;
	}
	public void setAddedStop(boolean addedStop) {
		this.addedStop = addedStop;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
